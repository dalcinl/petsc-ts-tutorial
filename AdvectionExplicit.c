#include <petsc.h>

typedef struct {
  PetscReal a;
} AppCtx;

PetscReal Sine(PetscReal y) {
  return PetscSinReal(4*PETSC_PI*y);
}

PetscReal MexHat(PetscReal y) {
  PetscReal t = y-0.5;
  PetscReal s = 0.1;
  PetscReal A = 2/(PetscSqrtReal(3*s)*PetscPowReal(s,0.25));
  return A*(1-PetscSqr(t/s)) * PetscExpReal(-0.5*PetscSqr(t/s));
}

#define Shape Sine

#undef  __FUNCT__
#define __FUNCT__ "Solution"
PetscErrorCode Solution(TS ts,PetscReal t,Vec U,void *ctx)
{
  AppCtx         *app = (AppCtx*)ctx;
  DM             dm;
  DMDALocalInfo  info;
  PetscInt       i,xs,xm;
  PetscReal      h, a = app->a;
  PetscScalar    *u;
  PetscErrorCode ierr;

  PetscFunctionBegin;
  ierr = TSGetDM(ts,&dm);CHKERRQ(ierr);
  ierr = DMDAGetLocalInfo(dm,&info);CHKERRQ(ierr);
  xs = info.xs; xm = info.xm; h = 1.0/(info.mx-1);
  ierr = DMDAVecGetArray(dm,U,&u);CHKERRQ(ierr);
  for (i=xs; i<xs+xm; i++)
    u[i] = Shape(i*h - a*t);
  ierr = DMDAVecRestoreArray(dm,U,&u);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef  __FUNCT__
#define __FUNCT__ "RHSFunction"
PetscErrorCode RHSFunction(TS ts,PetscReal t,Vec U,Vec R,void *ctx)
{
  AppCtx            *app = (AppCtx*)ctx;
  DM                dm;
  DMDALocalInfo     info;
  Vec               localU;
  const PetscScalar *u;
  PetscScalar       *rhs;
  PetscInt          i,xs,xm;
  PetscReal         h, a = app->a;
  PetscErrorCode    ierr;

  PetscFunctionBegin;
  ierr = TSGetDM(ts,&dm);CHKERRQ(ierr);
  ierr = DMDAGetLocalInfo(dm,&info);CHKERRQ(ierr);
  xs = info.xs; xm = info.xm; h = 1.0/(info.mx-1);

  ierr = DMGetLocalVector(dm,&localU);CHKERRQ(ierr);
  ierr = DMGlobalToLocalBegin(dm,U,INSERT_VALUES,localU);CHKERRQ(ierr);
  ierr = DMGlobalToLocalEnd  (dm,U,INSERT_VALUES,localU);CHKERRQ(ierr);
  
  ierr = DMDAVecGetArrayRead(dm,localU,&u);CHKERRQ(ierr);
  ierr = DMDAVecGetArray(dm,R,&rhs);CHKERRQ(ierr);

  for (i=xs; i<xs+xm; i++)
    rhs[i] = -a*(u[i+1]-u[i-1])/(2*h);
  
  ierr = DMDAVecRestoreArrayRead(dm,localU,&u);CHKERRQ(ierr);
  ierr = DMDAVecRestoreArray(dm,R,&rhs);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef  __FUNCT__
#define __FUNCT__ "main"
int main(int argc, char *argv[])
{
  PetscInt       N = 100;
  AppCtx         app = {/*a*/ 1};
  DM             dm;
  TS             ts;
  Vec            U;
  PetscErrorCode ierr;

  ierr = PetscInitialize(&argc,&argv,NULL,NULL);CHKERRQ(ierr);

  ierr = PetscOptionsBegin(PETSC_COMM_WORLD,"","Advection Options","TS");CHKERRQ(ierr);
  ierr = PetscOptionsInt ("-n","Grid size",__FILE__,N,&N,NULL);CHKERRQ(ierr);
  ierr = PetscOptionsReal("-a","Advection",__FILE__,app.a,&app.a,NULL);CHKERRQ(ierr);
  ierr = PetscOptionsEnd();CHKERRQ(ierr);

  ierr = DMDACreate1d(PETSC_COMM_WORLD,DM_BOUNDARY_PERIODIC,N,1,1,NULL,&dm);CHKERRQ(ierr);

  ierr = TSCreate(PETSC_COMM_WORLD,&ts);CHKERRQ(ierr);
  ierr = TSSetDuration(ts,PETSC_MAX_INT,2.0);CHKERRQ(ierr);
  ierr = TSSetTimeStep(ts,0.5/(N-1)/app.a);CHKERRQ(ierr);
  ierr = TSSetExactFinalTime(ts,TS_EXACTFINALTIME_STEPOVER);CHKERRQ(ierr);
  ierr = TSSetDM(ts,dm);CHKERRQ(ierr);

  ierr = TSSetRHSFunction(ts,NULL,RHSFunction,&app);CHKERRQ(ierr);

  ierr = DMCreateGlobalVector(dm,&U);CHKERRQ(ierr);
  ierr = Solution(ts,0.0,U,&app);CHKERRQ(ierr);
  
  ierr = TSSetFromOptions(ts);CHKERRQ(ierr);
  ierr = TSSolve(ts,U);CHKERRQ(ierr);

  ierr = VecDestroy(&U);CHKERRQ(ierr);
  ierr = TSDestroy(&ts);CHKERRQ(ierr);
  ierr = DMDestroy(&dm);CHKERRQ(ierr);
  ierr = PetscFinalize();CHKERRQ(ierr);
  return 0;
}
