#include <petsc.h>

typedef struct {
  PetscReal Omega;   /* natural frequency */
  PetscReal Xi;      /* damping coefficient  */
  PetscReal f;       /* constant forcing */
  PetscReal u0,v0;   /* initial conditions */
} AppCtx;

#undef  __FUNCT__
#define __FUNCT__ "IFunction"
PetscErrorCode IFunction(TS ts,PetscReal t,Vec X,Vec Xdot,
                         Vec R,void *ctx)
{
  AppCtx         *app = (AppCtx*)ctx;
  PetscReal      Omega = app->Omega, Xi = app->Xi, f = app->f;
  PetscErrorCode ierr;

  PetscFunctionBegin;
  const PetscScalar *x,*x_t; PetscScalar *res;
  ierr = VecGetArrayRead(Xdot,&x_t);CHKERRQ(ierr);
  ierr = VecGetArrayRead(X,&x);CHKERRQ(ierr);
  ierr = VecGetArray(R,&res);CHKERRQ(ierr);

  PetscScalar u_t = x_t[0], u = x[0];
  PetscScalar v_t = x_t[1], v = x[1];
  
  res[0] = u_t - v;
  res[1] = v_t + 2*Xi*Omega*v + Omega*Omega*u - f;

  ierr = VecRestoreArrayRead(Xdot,&x_t);CHKERRQ(ierr);
  ierr = VecRestoreArrayRead(X,&x);CHKERRQ(ierr);
  ierr = VecRestoreArray(R,&res);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef  __FUNCT__
#define __FUNCT__ "IJacobian"
PetscErrorCode IJacobian(TS ts,PetscReal t,Vec X,Vec Xdot,
                         PetscReal alpha,Mat J,Mat P,void *ctx)
{
  AppCtx         *app = (AppCtx*)ctx;
  PetscReal      Omega = app->Omega, Xi = app->Xi;
  PetscErrorCode ierr;

  PetscFunctionBegin;

  PetscReal Jac[2][2];
  Jac[0][0] = alpha;       Jac[0][1] = -1;
  Jac[1][0] = Omega*Omega; Jac[1][1] = alpha + 2*Xi*Omega;

  PetscInt idx[2] = {0, 1};
  ierr = MatSetValues(J,2,idx,2,idx,&Jac[0][0],INSERT_VALUES);CHKERRQ(ierr);
  ierr = MatAssemblyBegin(J,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  ierr = MatAssemblyEnd(J,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef  __FUNCT__
#define __FUNCT__ "main"
int main(int argc, char *argv[])
{
  PetscErrorCode ierr;
  ierr = PetscInitialize(&argc,&argv,NULL,NULL);CHKERRQ(ierr);

  AppCtx app = {/*Omega*/ 1, /*Xi*/ 0, /*f*/ 0, /*u0*/ 1, /*v0*/ 0};
  ierr = PetscOptionsBegin(PETSC_COMM_SELF,"","Oscillator Options","TS");CHKERRQ(ierr);
  ierr = PetscOptionsReal("-omega","Natural frequency",__FILE__,app.Omega,&app.Omega,NULL);CHKERRQ(ierr);
  ierr = PetscOptionsReal("-xi","Damping coefficient",__FILE__,app.Xi,&app.Xi,NULL);CHKERRQ(ierr);
  ierr = PetscOptionsReal("-f","Constant forcing",__FILE__,app.f,&app.f,NULL);CHKERRQ(ierr);
  ierr = PetscOptionsReal("-u0","Initial condition u0",__FILE__,app.u0,&app.u0,NULL);CHKERRQ(ierr);
  ierr = PetscOptionsReal("-v0","Initial condition v0",__FILE__,app.v0,&app.v0,NULL);CHKERRQ(ierr);
  ierr = PetscOptionsEnd();CHKERRQ(ierr);

  TS ts;
  ierr = TSCreate(PETSC_COMM_SELF,&ts);CHKERRQ(ierr);
  ierr = TSSetDuration(ts,PETSC_MAX_INT,4*(2*PETSC_PI)/app.Omega);CHKERRQ(ierr);
  ierr = TSSetTimeStep(ts,0.1);CHKERRQ(ierr);
  ierr = TSSetExactFinalTime(ts,TS_EXACTFINALTIME_STEPOVER);CHKERRQ(ierr);

  Vec F;
  ierr = VecCreateSeq(PETSC_COMM_SELF,2,&F);CHKERRQ(ierr);
  ierr = TSSetIFunction(ts,F,IFunction,&app);CHKERRQ(ierr);
  
  Mat J;
  ierr = MatCreateSeqDense(PETSC_COMM_SELF,2,2,NULL,&J);CHKERRQ(ierr);
  ierr = TSSetIJacobian(ts,J,J,IJacobian,&app);CHKERRQ(ierr);

  Vec X; PetscScalar *x;
  ierr = VecCreateSeq(PETSC_COMM_SELF,2,&X);CHKERRQ(ierr);
  ierr = VecGetArray(X,&x);CHKERRQ(ierr);
  x[0] = app.u0; x[1] = app.v0;
  ierr = VecRestoreArray(X,&x);CHKERRQ(ierr);
  ierr = TSSetSolution(ts,X);CHKERRQ(ierr);

  ierr = TSSetFromOptions(ts);CHKERRQ(ierr);
  ierr = TSSolve(ts,X);CHKERRQ(ierr);

  ierr = VecDestroy(&F);CHKERRQ(ierr);
  ierr = MatDestroy(&J);CHKERRQ(ierr);
  ierr = VecDestroy(&X);CHKERRQ(ierr);
  ierr = TSDestroy(&ts);CHKERRQ(ierr);
  ierr = PetscFinalize();CHKERRQ(ierr);
  return 0;
}
