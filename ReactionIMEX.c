#include <petsc.h>
/*
  Solves the pair of PDEs:
    u_t = D_1 \nabla^2 u + f(u,v)
    v_t = D_2 \nabla^2 v + g(u,v)

  where:
    f(u,v) := \alpha u (1 - \tau_1 v^2) + v (1 - \tau_2 u)
    g(u,v) := \beta v (1 + \alpha \frac{\tau_1}{\beta} u v) + u (\gamma + \tau_2 v)
*/

/*
{.delta=0.0045, .tau1=0.02, .tau2=0.2, .alpha=0.899, .beta=-0.91, gamma=-.alpha};
{.delta=0.0045, .tau1=0.02, .tau2=0.2, .alpha=1.9,   .beta=-0.91, gamma=-.alpha};
{.delta=0.0045, .tau1=2.02, .tau2=0,   .alpha=2.0,   .beta=-0.91, gamma=-.alpha};
{.delta=0.0021, .tau1=3.50, .tau2=0,   .alpha=0.899, .beta=-0.91, gamma=-.alpha};
{.delta=0.0045, .tau1=0.02, .tau2=0.2, .alpha=1.9,   .beta=-0.85, gamma=-.alpha};
{.delta=0.0001, .tau1=0.02, .tau2=0.2, .alpha=0.899, .beta=-0.91, gamma=-.alpha};
{.delta=0.0005, .tau1=2.02, .tau2=0.,  .alpha=2.0,   .beta=-0.91, gamma=-.alpha};
*/

typedef struct {
  PetscReal D1,D2;
  PetscReal delta;
  PetscReal alpha;
  PetscReal beta;
  PetscReal gamma;
  PetscReal tau1;
  PetscReal tau2;
} AppCtx;

typedef struct {
  PetscScalar u,v;
} Field;

PetscErrorCode IFunction(TS ts,PetscReal t,Vec X,Vec Xdot,Vec R,void *ctx);
PetscErrorCode RHSFunction(TS ts,PetscReal t,Vec X,Vec R,void *ctx);
PetscErrorCode IJacobian(TS ts,PetscReal t,Vec X,Vec Xdot,PetscReal shift,Mat J,Mat P,void *ctx);

#undef  __FUNCT__
#define __FUNCT__ "main"
int main(int argc, char *argv[])
{
  PetscInt       N = 30;
  AppCtx         app;
  DM             dm;
  TS             ts;
  Vec            U;
  PetscErrorCode ierr;

  ierr = PetscInitialize(&argc,&argv,NULL,NULL);CHKERRQ(ierr);

  app.D1    =  0.500;
  app.D2    =  1.000;
  app.delta =  0.0045;
  app.alpha =  0.899;
  app.beta  = -0.910;
  app.gamma = -app.alpha;
  app.tau1  =  0.020;
  app.tau2  =  0.200;

  ierr = PetscOptionsBegin(PETSC_COMM_WORLD,"","Advection Options","TS");CHKERRQ(ierr);
  ierr = PetscOptionsInt ("-n","Grid sizes",__FILE__,N,&N,NULL);CHKERRQ(ierr);
  ierr = PetscOptionsEnd();CHKERRQ(ierr);

  ierr = DMDACreate2d(PETSC_COMM_WORLD,
                      DM_BOUNDARY_PERIODIC,DM_BOUNDARY_PERIODIC,
                      DMDA_STENCIL_STAR,
                      N,N,
                      PETSC_DECIDE,PETSC_DECIDE,
                      /*dof*/2,/*sw*/1,
                      NULL,NULL,
                      &dm);CHKERRQ(ierr);
  ierr = DMDASetFieldName(dm,0,"u");CHKERRQ(ierr);
  ierr = DMDASetFieldName(dm,1,"v");CHKERRQ(ierr);

  ierr = TSCreate(PETSC_COMM_WORLD,&ts);CHKERRQ(ierr);
  ierr = TSSetDuration(ts,PETSC_MAX_INT,1e3);CHKERRQ(ierr);
  ierr = TSSetTimeStep(ts,1e-3);CHKERRQ(ierr);
  ierr = TSSetExactFinalTime(ts,TS_EXACTFINALTIME_STEPOVER);CHKERRQ(ierr);
  ierr = TSSetType(ts,TSARKIMEX);CHKERRQ(ierr);

  ierr = TSSetDM(ts,dm);CHKERRQ(ierr);
  ierr = TSSetIFunction(ts,NULL,IFunction,&app);CHKERRQ(ierr);
  ierr = TSSetRHSFunction(ts,NULL,RHSFunction,&app);CHKERRQ(ierr);
  ierr = TSSetIJacobian(ts,NULL,NULL,IJacobian,&app);CHKERRQ(ierr);

  ierr = DMCreateGlobalVector(dm,&U);CHKERRQ(ierr);
  ierr = VecSetRandom(U,NULL);CHKERRQ(ierr);

  ierr = TSSetFromOptions(ts);CHKERRQ(ierr);
  ierr = TSSolve(ts,U);CHKERRQ(ierr);

  ierr = VecDestroy(&U);CHKERRQ(ierr);
  ierr = TSDestroy(&ts);CHKERRQ(ierr);
  ierr = DMDestroy(&dm);CHKERRQ(ierr);
  ierr = PetscFinalize();CHKERRQ(ierr);
  return 0;
}

#undef  __FUNCT__
#define __FUNCT__ "IFunction"
PetscErrorCode IFunction(TS ts,PetscReal t,Vec X,Vec Xdot,Vec R,void *ctx)
{
  DM              dm;
  DMDALocalInfo   info;
  Vec             localX;
  const Field     **x,**x_t;
  Field           **r;
  PetscInt        i,xs,xm,mx;
  PetscInt        j,ys,ym,my;
  PetscReal       hx,hy;
  PetscErrorCode  ierr;

  AppCtx          *app  = (AppCtx*)ctx;
  PetscReal       D1    = app->D1;
  PetscReal       D2    = app->D2;
  PetscReal       delta = app->delta;

  PetscFunctionBegin;
  ierr = TSGetDM(ts,&dm);CHKERRQ(ierr);
  ierr = DMDAGetLocalInfo(dm,&info);CHKERRQ(ierr);
  xs = info.xs; xm = info.xm; mx = info.mx; hx = 1.0/(mx-1);
  ys = info.ys; ym = info.ym; my = info.my; hy = 1.0/(my-1);

  ierr = DMGetLocalVector(dm,&localX);CHKERRQ(ierr);
  ierr = DMGlobalToLocalBegin(dm,X,INSERT_VALUES,localX);CHKERRQ(ierr);
  ierr = DMGlobalToLocalEnd  (dm,X,INSERT_VALUES,localX);CHKERRQ(ierr);

  ierr = DMDAVecGetArrayRead(dm,Xdot,&x_t);CHKERRQ(ierr);
  ierr = DMDAVecGetArrayRead(dm,localX,&x);CHKERRQ(ierr);
  ierr = DMDAVecGetArray(dm,R,&r);CHKERRQ(ierr);

  for (j=ys; j<ys+ym; j++) {
    for (i=xs; i<xs+xm; i++) {
      PetscScalar u_t = x_t[j][i].u;
      PetscScalar v_t = x_t[j][i].v;

      PetscScalar u_xx = (2*x[j][i].u - x[j][i-1].u - x[j][i+1].u)/(hx*hx);
      PetscScalar u_yy = (2*x[j][i].u - x[j-1][i].u - x[j+1][i].u)/(hy*hy);
      PetscScalar v_xx = (2*x[j][i].v - x[j][i-1].v - x[j][i+1].v)/(hx*hx);
      PetscScalar v_yy = (2*x[j][i].v - x[j-1][i].v - x[j+1][i].v)/(hy*hy);

      r[j][i].u = u_t + delta*D1*(u_xx + u_yy);
      r[j][i].v = v_t + delta*D2*(v_xx + v_yy);
    }
  }

  ierr = DMDAVecRestoreArrayRead(dm,Xdot,&x_t);CHKERRQ(ierr);
  ierr = DMDAVecRestoreArrayRead(dm,localX,&x);CHKERRQ(ierr);
  ierr = DMDAVecRestoreArray(dm,R,&r);CHKERRQ(ierr);
  ierr = DMRestoreLocalVector(dm,&localX);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef  __FUNCT__
#define __FUNCT__ "RHSFunction"
PetscErrorCode RHSFunction(TS ts,PetscReal t,Vec X,Vec R,void *ctx)
{
  DM              dm;
  DMDALocalInfo   info;
  const Field     **x;
  Field           **r;
  PetscInt        i,xs,xm,mx;
  PetscInt        j,ys,ym,my;
  PetscReal       hx,hy;
  PetscErrorCode  ierr;

  AppCtx          *app  = (AppCtx*)ctx;
  PetscReal       alpha = app->alpha;
  PetscReal       beta  = app->beta;
  PetscReal       gamma = app->gamma;
  PetscReal       tau1  = app->tau1;
  PetscReal       tau2  = app->tau2;

  PetscFunctionBegin;
  ierr = TSGetDM(ts,&dm);CHKERRQ(ierr);
  ierr = DMDAGetLocalInfo(dm,&info);CHKERRQ(ierr);
  xs = info.xs; xm = info.xm; mx = info.mx; hx = 1.0/(mx-1);
  ys = info.ys; ym = info.ym; my = info.my; hy = 1.0/(my-1);

  ierr = DMDAVecGetArrayRead(dm,X,&x);CHKERRQ(ierr);
  ierr = DMDAVecGetArray(dm,R,&r);CHKERRQ(ierr);

  for (j=ys; j<ys+ym; j++) {
    for (i=xs; i<xs+xm; i++) {
      PetscScalar u = x[j][i].u;
      PetscScalar v = x[j][i].v;
      PetscScalar f = alpha*u*(1-tau1*v*v) + v*(1-tau2*u);
      PetscScalar g = beta*v*(1+alpha*tau1/beta*u*v) + u*(gamma+tau2*v);

      r[j][i].u = f;
      r[j][i].v = g;
    }
  }

  ierr = DMDAVecRestoreArrayRead(dm,X,&x);CHKERRQ(ierr);
  ierr = DMDAVecRestoreArray(dm,R,&r);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef  __FUNCT__
#define __FUNCT__ "IJacobian"
PetscErrorCode IJacobian(TS ts,PetscReal t,Vec X,Vec Xdot,PetscReal shift,Mat J,Mat P,void *ctx)
{
  DM             dm;
  DMDALocalInfo  info;
  PetscInt       i,xs,xm,mx;
  PetscInt       j,ys,ym,my;
  PetscReal      hx,hy;
  PetscErrorCode ierr;

  AppCtx         *app  = (AppCtx*)ctx;
  PetscReal      D1    = app->D1;
  PetscReal      D2    = app->D2;
  PetscReal      delta = app->delta;

  PetscFunctionBegin;
  ierr = TSGetDM(ts,&dm);CHKERRQ(ierr);
  ierr = DMDAGetLocalInfo(dm,&info);CHKERRQ(ierr);
  xs = info.xs; xm = info.xm; mx = info.mx; hx = 1.0/(mx-1);
  ys = info.ys; ym = info.ym; my = info.my; hy = 1.0/(my-1);

  for (j=ys; j<ys+ym; j++) {
    for (i=xs; i<xs+xm; i++) {
      MatStencil  row,col[5];
      PetscScalar val[2][5][2];
      ierr = PetscMemzero(val,sizeof(val));CHKERRQ(ierr);

      row.i    = i;   row.j    = j;
      col[0].i = i-1; col[0].j = j;
      col[1].i = i+1; col[1].j = j;
      col[2].i = i;   col[2].j = j;
      col[3].i = i;   col[3].j = j-1;
      col[4].i = i;   col[4].j = j+1;

      val[0][0][0] = -delta*D1/(hx*hx);
      val[0][1][0] = -delta*D1/(hx*hx);
      val[0][2][0] = shift + delta*D1*(2/(hx*hx) + 2/(hy*hy));
      val[0][3][0] = -delta*D1/(hy*hy);
      val[0][4][0] = -delta*D1/(hy*hy);

      val[1][0][1] = -delta*D2/(hx*hx);
      val[1][1][1] = -delta*D2/(hx*hx);
      val[1][2][1] = shift + delta*D2*(2/(hx*hx) + 2/(hy*hy));
      val[1][3][1] = -delta*D2/(hy*hy);
      val[1][4][1] = -delta*D2/(hy*hy);

      ierr = MatSetValuesBlockedStencil(P,1,&row,5,col,&val[0][0][0],INSERT_VALUES);CHKERRQ(ierr);
    }
  }

  ierr = MatAssemblyBegin(P,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  ierr = MatAssemblyEnd  (P,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  if (J != P) {
    ierr = MatAssemblyBegin(J,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
    ierr = MatAssemblyEnd  (J,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  }
  PetscFunctionReturn(0);
}
