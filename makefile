ALL: all
include $(PETSC_DIR)/lib/petsc/conf/variables
include $(PETSC_DIR)/lib/petsc/conf/rules

OscillatorExplicit: OscillatorExplicit.o
	$(CLINKER) -o $@ $< $(PETSC_LIB)
	$(RM) $<

OscillatorImplicit: OscillatorImplicit.o
	$(CLINKER) -o $@ $< $(PETSC_LIB)
	$(RM) $<

AdvectionExplicit: AdvectionExplicit.o
	$(CLINKER) -o $@ $< $(PETSC_LIB)
	$(RM) $<

ReactionImplicit: ReactionImplicit.o
	$(CLINKER) -o $@ $< $(PETSC_LIB)
	$(RM) $<

ReactionIMEX: ReactionIMEX.o
	$(CLINKER) -o $@ $< $(PETSC_LIB)
	$(RM) $<

TARGETS = \
OscillatorExplicit \
OscillatorImplicit \
AdvectionExplicit \
ReactionImplicit \
ReactionIMEX

all:: $(TARGETS)
clean::
	-@$(RM) $(TARGETS)
